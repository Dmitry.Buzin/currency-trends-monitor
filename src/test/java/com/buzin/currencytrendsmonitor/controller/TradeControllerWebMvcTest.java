package com.buzin.currencytrendsmonitor.controller;

import com.buzin.currencytrendsmonitor.entity.TradeEntity;
import com.buzin.currencytrendsmonitor.mapper.TradeMapper;
import com.buzin.currencytrendsmonitor.service.TradeService;
import org.json.JSONObject;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import java.util.UUID;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.header;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@WebMvcTest(TradeController.class)
public class TradeControllerWebMvcTest {

    @MockBean
    private TradeService tradeService;
    @MockBean
    private TradeMapper tradeMapper;
    @Autowired
    private MockMvc mockMvc;

    @Test
    public void recordTrade_returns201() throws Exception {
        UUID uuid = UUID.randomUUID();
        when(tradeService.create(any())).thenReturn(TradeEntity.builder().id(uuid).build());

        mockMvc
                .perform(
                        post("/trade")
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(createDummyTradeMessageJsonObject().toString()))
                .andExpect(status().isCreated())
                .andExpect(header().stringValues("Location", "/trade/" + uuid));
    }

    private JSONObject createDummyTradeMessageJsonObject() {
        return new JSONObject();
    }

}
