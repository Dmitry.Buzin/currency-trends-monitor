package com.buzin.currencytrendsmonitor.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class TradeDto {

    private String id;

    private String userId;

    private String currencyFrom;

    private String currencyTo;

    private String amountSell;

    private String amountBuy;

    private String rate;

    private String timePlaced;

    private String originatingCountry;

}
