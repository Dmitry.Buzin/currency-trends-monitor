package com.buzin.currencytrendsmonitor.service;

import com.buzin.currencytrendsmonitor.entity.TradeEntity;
import com.buzin.currencytrendsmonitor.exception.EntityNotFoundException;
import com.buzin.currencytrendsmonitor.repository.TradeRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
@RequiredArgsConstructor
public class TradeService {

    private final TradeRepository repository;

    public TradeEntity create(TradeEntity entity) {
        return repository.save(entity);
    }

    public TradeEntity get(UUID uuid) {
        return repository.findById(uuid)
                .orElseThrow(() -> new EntityNotFoundException("Trade entity not found with id: " + uuid));
    }
}
