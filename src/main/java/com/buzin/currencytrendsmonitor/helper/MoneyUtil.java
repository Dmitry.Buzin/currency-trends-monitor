package com.buzin.currencytrendsmonitor.helper;

import lombok.experimental.UtilityClass;

@UtilityClass
public class MoneyUtil {

    public static long toMicroDollars(String amount) {
        return (long) (Double.parseDouble(amount) * 1_000_000);
    }

    public static double toDollars(long amount) {
        return amount / 1_000_000.0;
    }
}
