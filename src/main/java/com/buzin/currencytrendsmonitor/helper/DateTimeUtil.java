package com.buzin.currencytrendsmonitor.helper;

import lombok.experimental.UtilityClass;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeFormatterBuilder;

@UtilityClass
public class DateTimeUtil {

    public static final String TRADE_DATE_TIME_FORMAT_STRING = "dd-MMM-yy HH:mm:ss";
    public static final DateTimeFormatter tradeDateTimeFormat = new DateTimeFormatterBuilder()
            .parseCaseInsensitive()
            .appendPattern(TRADE_DATE_TIME_FORMAT_STRING)
            .toFormatter();

    public static ZonedDateTime dateTimeFromTradeString(String dateTimeString) {
        return LocalDateTime.parse(dateTimeString, DateTimeUtil.tradeDateTimeFormat).atZone(ZoneId.of("UTC"));
    }

}
