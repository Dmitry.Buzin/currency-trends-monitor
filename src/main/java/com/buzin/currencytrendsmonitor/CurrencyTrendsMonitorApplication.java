package com.buzin.currencytrendsmonitor;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CurrencyTrendsMonitorApplication {

    public static void main(String[] args) {
        SpringApplication.run(CurrencyTrendsMonitorApplication.class, args);
    }

}
