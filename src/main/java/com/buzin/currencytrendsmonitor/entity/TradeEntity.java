package com.buzin.currencytrendsmonitor.entity;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.Length;

import java.time.ZonedDateTime;
import java.util.UUID;

@Entity(name = "trade")
@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder
public class TradeEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.UUID)
    private UUID id;

    @Column(name = "user_id", nullable = false)
    @Length(max = 128, min = 1)
    private String userId;

    @Column(name = "currency_from", nullable = false)
    @Length(max = 3, min = 3)
    private String currencyFrom;

    @Column(name = "currency_to", nullable = false)
    @Length(max = 3, min = 3)
    private String currencyTo;

    @Column(name = "amount_sell", nullable = false)
    private Long amountSell;

    @Column(name = "amount_buy", nullable = false)
    private Long amountBuy;

    @Column(name = "rate", nullable = false)
    private Double rate;

    @Column(name = "time_placed", nullable = false)
    private ZonedDateTime timePlaced;

    @Column(name = "originating_country", nullable = false)
    @Length(max = 3, min = 2)
    private String originatingCountry;
}
