package com.buzin.currencytrendsmonitor.mapper;

import com.buzin.currencytrendsmonitor.dto.TradeDto;
import com.buzin.currencytrendsmonitor.entity.TradeEntity;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import static com.buzin.currencytrendsmonitor.helper.DateTimeUtil.TRADE_DATE_TIME_FORMAT_STRING;

@Mapper(componentModel = "spring")
public interface TradeMapper {

    @Mapping(source = "timePlaced", dateFormat = TRADE_DATE_TIME_FORMAT_STRING, target = "timePlaced")
    @Mapping(target = "amountSell", expression = "java(String.valueOf(com.buzin.currencytrendsmonitor.helper.MoneyUtil.toDollars(tradeEntity.getAmountSell())))")
    @Mapping(target = "amountBuy", expression = "java(String.valueOf(com.buzin.currencytrendsmonitor.helper.MoneyUtil.toDollars(tradeEntity.getAmountBuy())))")
    @Mapping(target = "rate", expression = "java(String.valueOf(tradeEntity.getRate()))")
    TradeDto tradeToTradeDto(TradeEntity tradeEntity);

    @Mapping(target = "amountSell", expression = "java(com.buzin.currencytrendsmonitor.helper.MoneyUtil.toMicroDollars(tradeDto.getAmountSell()))")
    @Mapping(target = "amountBuy", expression = "java(com.buzin.currencytrendsmonitor.helper.MoneyUtil.toMicroDollars(tradeDto.getAmountBuy()))")
    @Mapping(target = "rate", expression = "java(Double.parseDouble(tradeDto.getRate()))")
    @Mapping(target = "timePlaced", expression = "java(com.buzin.currencytrendsmonitor.helper.DateTimeUtil.dateTimeFromTradeString(tradeDto.getTimePlaced()))")
    TradeEntity tradeDtoToTradeEntity(TradeDto tradeDto);
}
