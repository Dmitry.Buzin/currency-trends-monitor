package com.buzin.currencytrendsmonitor.controller;

import com.buzin.currencytrendsmonitor.dto.TradeDto;
import com.buzin.currencytrendsmonitor.entity.TradeEntity;
import com.buzin.currencytrendsmonitor.mapper.TradeMapper;
import com.buzin.currencytrendsmonitor.service.TradeService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.util.UUID;

@RestController()
@RequestMapping("/trade")
@RequiredArgsConstructor
public class TradeController {

    private final TradeService tradeService;
    private final TradeMapper tradeMapper;

    @PostMapping
    public ResponseEntity<?> recordTrade(@RequestBody TradeDto tradeDto) {
        TradeEntity tradeEntity = tradeService.create(tradeMapper.tradeDtoToTradeEntity(tradeDto));
        return ResponseEntity.created(URI.create("/trade/" + tradeEntity.getId())).body("OK");
    }

    @GetMapping("/{uuid}")
    public ResponseEntity<?> get(@PathVariable UUID uuid) {
        return ResponseEntity.ok().body(tradeMapper.tradeToTradeDto(tradeService.get(uuid)));
    }
}
